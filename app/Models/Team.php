<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Team extends Model
{
    use HasFactory, SoftDeletes;

    public static function findTeamByDepartmentID($department_id) {
        $team = Team::where('department_id',$department_id)->get();
        return $team;
    }
}
