<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Permission extends Model
{
    use HasFactory, SoftDeletes;

    public static function getPermission($module_id = "") {
        // DB::enableQueryLog();
        $modules = DB::table('modules')->whereNull('deleted_at');
        (!empty($module_id)) ?
            $modules->where('id','=', $module_id) : null;

        $myData =  $modules->get();
        

        $data = array();
        foreach($myData as $m) {
            //search modules in permission by role
            $newArrModule = array();
            $newArrModule['module_id'] = $m->id;
            $newArrModule['module_name'] = $m->name;
            $newArrModule['role_can_access'] = [];
            $newArrModule['user_can_access'] = [];
            $permissionByRole = DB::table('permissions as p')
                                ->leftJoin('roles as r','p.role_id','=','r.id')
                                ->where('p.module_id', $m->id)
                                ->where('p.is_allow', 1)
                                ->selectRaw('r.*')
                                ->orderBy('r.name','ASC')
                                ->get();
            foreach($permissionByRole as $p) {
                $newArrModule['role_can_access']['role_id'][] = $p->id;
                $newArrModule['role_can_access']['role_name'][] = $p->name;
            }

            $permissionByUser = DB::table('permissions as p')
                                ->leftJoin('users as u','p.user_id','=','u.id')
                                ->where('p.module_id', $m->id)
                                ->where('p.is_allow', 1)
                                ->select('u.fullname as user_name')
                                ->orderBy('u.fullname','ASC')
                                ->get();
            foreach($permissionByUser as $p) {
                $newArrModule['user_can_access'][] = $p->user_name;
            }
            $data[] = $newArrModule;
        }

        return $data;
    }

    public static function updateData($module_id,$role_can_access) {

        // set active where role_can_accsess

        if(!is_null($role_can_access)) {
            for($i=0; $i<count($role_can_access); $i++) {
                
                $permissions = DB::table('permissions as p')
                                ->where('role_id',$role_can_access[$i])
                                ->where('module_id',$module_id);
                if($permissions->count() == 0 ) {
                    $permission = new Permission();
                    $permission->module_id = $module_id;
                    $permission->role_id = $role_can_access[$i];
                    $permission->is_allow = 1;
                    $permission->save();
                } else {
                    Permission::where('id',$permissions->first()->id)->update([
                        'is_allow' => 1,
                        'updated_at' => date('Y-m-d H:i:s'),
                    ]);
                    
                }
            }
    
            //set not active
            $permissions = Permission::where('module_id',$module_id)
                                        ->whereNotIn('role_id',$role_can_access)
                                        ->update([
                                            'is_allow' => 0,
                                        ]);
        } else {
            $permissions = Permission::where('module_id',$module_id)
                                        ->update([
                                            'is_allow' => 0,
                                        ]);
        }
    }

    public static function getPermissionModuleCanAccessByUser($user_id,$role_id) {
        $module = [];
        $permissions = Permission::join('modules as m','permissions.module_id','=','m.id')
                                ->where('user_id',$user_id)
                                ->orWhere('role_id',$role_id)
                                ->select('m.name')->get();
        foreach($permissions as $permission) {
            array_push($module, $permission->name);
        }
        return $module;
    }
}