<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Department extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'name',
        'company_id'
    ];

    public static function findDepartmentByCompanyID($company_id) {
        return Department::join('companies as c','departments.company_id','=','c.id')
                                    ->where('c.id',$company_id)
                                    ->selectRaw('departments.*,c.name as company_name')
                                    ->get();
    }
}
