<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'fullname',
        'email',
        'role_id',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    public static function getUser()
    {
        // $user = DB::table('users as u')
        //     ->join('teams as t', 'u.team_id', '=', 't.id')
        //     ->join('departments as d', 't.department_id', '=', 'd.id')
        //     ->join('companies as c', 'd.company_id', '=', 'c.id')
        //     ->join('roles as r', 'u.role_id', '=', 'r.id')
        //     ->selectRaw('u.*,r.name as role_name,t.name as team_name,d.id as department_id,d.name as department_name,c.id as company_id,c.name as company_name')
        //     ->get();
        $user = DB::table('users as u')
            ->join('roles as r', 'u.role_id', '=', 'r.id')
            ->selectRaw('u.*,r.name as role_name')
            ->get();
        return $user;
    }

    public static function getUserByID($id)
    {
        // $user = DB::table('users as u')
        //     ->join('teams as t', 'u.team_id', '=', 't.id')
        //     ->join('departments as d', 't.department_id', '=', 'd.id')
        //     ->join('companies as c', 'd.company_id', '=', 'c.id')
        //     ->join('roles as r', 'u.role_id', '=', 'r.id')
        //     ->selectRaw('u.*,r.name as role_name,t.name as team_name,d.id as department_id,d.name as department_name,c.id as company_id,c.name as company_name')
        //     ->where('u.id', $id)
        //     ->first();
        $user = DB::table('users as u')
            ->join('roles as r', 'u.role_id', '=', 'r.id')
            ->selectRaw('u.*,r.name as role_name')
            ->where('u.id', $id)
            ->first();
        return $user;
    }
}
