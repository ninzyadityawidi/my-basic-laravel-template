<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class Modals extends Component
{
    /**
     * Create a new component instance.
     */

    public $modalId;
    public $title;
    public $size;

    public function __construct($modalId = "",$title = "", $size = "lg")
    {
        $this->modalId = $modalId;
        $this->title = $title;
        $this->size = $size;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.modals');
    }
}
