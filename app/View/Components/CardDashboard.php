<?php

namespace App\View\Components;

use Closure;
use Illuminate\Contracts\View\View;
use Illuminate\View\Component;

class CardDashboard extends Component
{
    /**
     * Create a new component instance.
     */
    public $label;
    public $value;
    public $precentage;
    public $colorChart;
    public $colorPrecentage;
    public function __construct($label="", $value="",$precentage=0,$colorChart="",$colorPrecentage="")
    {
        $this->label = $label;
        $this->value = $value;
        $this->precentage = $precentage;
        $this->colorChart = $colorChart;
        $this->colorPrecentage = $colorPrecentage;
    }

    /**
     * Get the view / contents that represent the component.
     */
    public function render(): View|Closure|string
    {
        return view('components.card-dashboard');
    }
}
