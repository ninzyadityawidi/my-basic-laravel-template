<?php

namespace App\Http\Controllers;

use App\Models\Module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ModuleController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next){
            if(!hasPermission("SETTING [MODULE]")) {
                return redirect()->route('forbidden');
            }
            return $next($request);
        });
    }
    public function index() {
        $modules = Module::orderBy('name','asc')->get();
        return view('module.index',compact('modules'));
    }

    public function find(Request $request) {
        $module = Module::find($request->id);
        if($module) {
            return response([
                'success' => true,
                'data'    => $module
            ]);
        }

        return response([
            'success' => false,
            'msg'     => 'Data not found'
        ]);
    }

    public function store(Request $request) {
        $name    = $request->input("name");

        //validation header
        $rules = array(
            'name'          => 'required',
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $module            = new Module();
        $module->name      = strtoupper($name);
        $module->save();

        return response([
            'success'    => true,
            'msg'        => 'success', 
            'validation' => true,
            'callback'   => url('module'),                                                                                                                            
        ]);

    }

    public function update(Request $request) {
        $name    = $request->input("name");

        //validation header
        $rules = array(
            'name'          => 'required',
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $module  = Module::find($request->input('id'));
        if($module) {
            $module->update([
                'name'=> strtoupper($name),
            ]);

            return response([
                'success'    => true,
                'msg'        => 'success',
                'callback'   => url('module'),                                                                                                                            
            ]);
        }
        
        return response([
            'success'    => false,
            'msg'        => 'Failed, info your IT Development',
            'callback'   => url('module'),                                                                                                                            
        ]);

    }

    public function delete(Request $request) {
        $id = $request->input('id');
        $module = Module::find($id);
        if($module) {
            $module->delete();
            return response([
                'success'    => true,
                'msg'        => 'success', 
                'validation' => true,
                'callback'   => url('module'),                                                                                                                            
            ]);
        } else {
            return response([
                'success'    => false,
                'msg'        => 'failed, data not found',                                                                                                                               
            ]);
        }

    }
}
