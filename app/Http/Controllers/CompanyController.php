<?php

namespace App\Http\Controllers;

use App\Models\Company;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CompanyController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next){
            if(!hasPermission("MASTER [COMPANY]")) {
                return redirect()->route('forbidden');
            }
            return $next($request);
        });

    }
    public function index() {
        $companies = Company::all();
        return view("company.index", compact("companies"));
    }

    public function find(Request $request) {
        $company = Company::find($request->id);
        if($company) {
            return response([
                'success' => true,
                'data'    => $company
            ]);
        }

        return response([
            'success' => false,
            'msg'     => 'Data not found'
        ]);
    }

    public function store(Request $request) {
        $name = $request->input("name");
        $telephone = $request->input("telephone");
        $address = $request->input("address");

        //validation header
        $rules = array(
            'name'          => 'required',
            'telephone'     => 'required',
            'address'       => 'required',
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $company            = new Company();
        $company->name      = $name;
        $company->telephone = $telephone;
        $company->address   = $address;
        $company->save();

        return response([
            'success'    => true,
            'msg'        => 'success', 
            'validation' => true,
            'callback'   => url('company'),                                                                                                                            
        ]);

    }

    public function update(Request $request) {
        $name = $request->input("name");
        $telephone = $request->input("telephone");
        $address = $request->input("address");

        //validation header
        $rules = array(
            'name'          => 'required',
            'telephone'     => 'required',
            'address'       => 'required',
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $company            = Company::find($request->input('id'));
        if($company) {
            $company->update([
                'name'=> $name,
                'telephone' => $telephone,
                'address' => $address,
            ]);

            return response([
                'success'    => true,
                'msg'        => 'success',
                'callback'   => url('company'),                                                                                                                            
            ]);
        }
        
        return response([
            'success'    => false,
            'msg'        => 'Failed, info your IT Development',
            'callback'   => url('company'),                                                                                                                            
        ]);

    }

    public function delete(Request $request) {
        $id = $request->input('id');
        $company = Company::find($id);
        if($company) {
            $company->delete();
            return response([
                'success'    => true,
                'msg'        => 'success', 
                'validation' => true,
                'callback'   => url('company'),                                                                                                                            
            ]);
        } else {
            return response([
                'success'    => false,
                'msg'        => 'failed, data not found',                                                                                                                               
            ]);
        }

    }
}
