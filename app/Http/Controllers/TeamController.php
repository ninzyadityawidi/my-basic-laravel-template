<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Department;
use App\Models\Team;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TeamController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next){
            if(!hasPermission("MASTER [TEAM]")) {
                return redirect()->route('forbidden');
            }
            return $next($request);
        });
    }
    public function index() {
        $teams = Team::join('departments as d','teams.department_id','=','d.id')
                        ->join('companies as c','d.company_id','=','c.id')
                        ->selectRaw('teams.*,d.name as department_name,c.name as company_name')
                        ->get();
        $companies = Company::all();
        return view('team.index', compact('teams','companies'));
    }

    public function findDepartmentByCompanyID(Request $request) {
        $id = $request->id;
        $department = Department::findDepartmentByCompanyID($id);
        return response()->json($department);
    }

    public function find(Request $request) {
        $team = Team::join('departments as d','teams.department_id','=','d.id')
                        ->join('companies as c','d.company_id','=','c.id')
                        ->selectRaw('teams.*,d.name as department_name,c.id as company_id,c.name as company_name')
                        ->where('teams.id',$request->id)
                        ->first();
        if($team) {
            return response([
                'success' => true,
                'data'    => $team
            ]);
        }

        return response([
            'success' => false,
            'msg'     => 'Data not found'
        ]);
    }

    public function store(Request $request) {
        $name          = $request->input("name");
        $department_id = $request->input("department_id");

        //validation header
        $rules = array(
            'name'          => 'required',
            'department_id' => 'required',
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $team               = new Team();
        $team->name         = $name;
        $team->department_id= $department_id;
        $team->save();

        return response([
            'success'    => true,
            'msg'        => 'success', 
            'validation' => true,
            'callback'   => url('team'),                                                                                                                            
        ]);

    }

    public function update(Request $request) {
        $name          = $request->input("name");
        $department_id = $request->input("department_id");

        //validation header
        $rules = array(
            'name'          => 'required',
            'department_id' => 'required'
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $update  = Team::where('id',$request->input('id'))->update([
            'name'      => $name,
            'department_id'=> $department_id,
        ]);
        if($update) {

            return response([
                'success'    => true,
                'msg'        => 'success',
                'callback'   => url('team'),                                                                                                                            
            ]);
        }
        
        return response([
            'success'    => false,
            'msg'        => 'Failed, info your IT Development',
            'callback'   => url('team'),                                                                                                                            
        ]);

    }

    public function delete(Request $request) {
        $id = $request->input('id');
        $team = Team::find($id);
        if($team) {
            $team->delete();
            return response([
                'success'    => true,
                'msg'        => 'success', 
                'validation' => true,
                'callback'   => url('team'),                                                                                                                            
            ]);
        } else {
            return response([
                'success'    => false,
                'msg'        => 'failed, data not found',                                                                                                                               
            ]);
        }

    }
}
