<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class DepartmentController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next){
            if(!hasPermission("MASTER [DEPARTMENT]")) {
                return redirect()->route('forbidden');
            }
            return $next($request);
        });
    }
    public function index() {
        $companies   = Company::all();
        $departments = Department::join('companies','departments.company_id','=','companies.id')->selectRaw('departments.*,companies.name as company_name')->get();

        return view('department.index',compact('companies','departments'));
    }

    public function find(Request $request) {
        $department = Department::find($request->id);
        if($department) {
            return response([
                'success' => true,
                'data'    => $department
            ]);
        }

        return response([
            'success' => false,
            'msg'     => 'Data not found'
        ]);
    }

    public function store(Request $request) {
        $name    = $request->input("name");
        $company = $request->input("company");

        //validation header
        $rules = array(
            'name'          => 'required',
            'company'       => 'required'
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $department            = new Department();
        $department->name      = $name;
        $department->company_id= $company;
        $department->save();

        return response([
            'success'    => true,
            'msg'        => 'success', 
            'validation' => true,
            'callback'   => url('department'),                                                                                                                            
        ]);

    }

    public function update(Request $request) {
        $name    = $request->input("name");
        $company = $request->input("company");

        //validation header
        $rules = array(
            'name'          => 'required',
            'company'       => 'required'
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $department  = Department::find($request->input('id'));
        if($department) {
            $department->update([
                'name'=> $name,
                'company_id'=> $company,
            ]);

            return response([
                'success'    => true,
                'msg'        => 'success',
                'callback'   => url('department'),                                                                                                                            
            ]);
        }
        
        return response([
            'success'    => false,
            'msg'        => 'Failed, info your IT Development',
            'callback'   => url('department'),                                                                                                                            
        ]);

    }

    public function delete(Request $request) {
        $id = $request->input('id');
        $department = Department::find($id);
        if($department) {
            $department->delete();
            return response([
                'success'    => true,
                'msg'        => 'success', 
                'validation' => true,
                'callback'   => url('department'),                                                                                                                            
            ]);
        } else {
            return response([
                'success'    => false,
                'msg'        => 'failed, data not found',                                                                                                                               
            ]);
        }

    }
}
