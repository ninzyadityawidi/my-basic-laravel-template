<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next){
            if(!hasPermission("MASTER [ROLE]")) {
                return redirect()->route('forbidden');
            }
            return $next($request);
        });
    }
    public function index() {
        $roles = Role::all();
        return view("roles.index", compact("roles"));
    }

    public function find(Request $request) {
        $role = Role::find($request->id);
        if($role) {
            return response([
                'success' => true,
                'data'    => $role
            ]);
        }

        return response([
            'success' => false,
            'msg'     => 'Data not found'
        ]);
    }

    public function store(Request $request) {
        $name = $request->input("name");

        //validation header
        $rules = array(
            'name'          => 'required'
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $role            = new Role();
        $role->name      = $name;
        $role->save();

        return response([
            'success'    => true,
            'msg'        => 'success', 
            'validation' => true,
            'callback'   => url('role'),                                                                                                                            
        ]);

    }

    public function update(Request $request) {
        $name = $request->input("name");

        //validation header
        $rules = array(
            'name'          => 'required',
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $role            = Role::find($request->input('id'));
        if($role) {
            $role->update([
                'name'=> $name,
            ]);

            return response([
                'success'    => true,
                'msg'        => 'success',
                'callback'   => url('role'),                                                                                                                            
            ]);
        }
        
        return response([
            'success'    => false,
            'msg'        => 'Failed, info your IT Development',
            'callback'   => url('role'),                                                                                                                            
        ]);

    }

    public function delete(Request $request) {
        $id = $request->input('id');
        $role = Role::find($id);
        if($role) {
            $role->delete();
            return response([
                'success'    => true,
                'msg'        => 'success', 
                'validation' => true,
                'callback'   => url('role'),                                                                                                                            
            ]);
        } else {
            return response([
                'success'    => false,
                'msg'        => 'failed, data not found',                                                                                                                               
            ]);
        }

    }
}
