<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;

class Authentication extends Controller
{
    public function loginValidation(Request $request) {
        
        $validator = Validator::make($request->all(), [
            'email'    => 'required',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(),
                'validation' => true,
            ]);
        }


        $user = User::where('email',$request->email)->first();

        if($user) {

            if(Hash::check($request->password, $user->password)) {

                $dataSession = User::getUserByID($user->id);

                $module = Permission::getPermissionModuleCanAccessByUser($user->id,$dataSession->role_id);
                //set session
                session([
                    'id' => $dataSession->id,
                    'role_id' => $dataSession->role_id,
                    'role_name' => $dataSession->role_name,
                    // 'company_id' => $dataSession->company_id,
                    // 'company_name' => $dataSession->company_name,
                    // 'department_id' => $dataSession->department_id,
                    // 'department_name' => $dataSession->department_name,
                    // 'team_id' => $dataSession->team_id,
                    // 'team_name' => $dataSession->team_name,
                    'fullname' => $dataSession->fullname,
                    'email' => $dataSession->email,
                    'module' => $module,
                ]);

                return response([
                    'success'  => true,
                    'msg'      => 'Success',
                    'callback' => url('dashboard'),
                ]);
            }



        }

        return response([
            'success'  => false,
            'msg'      => 'Login Failed',
        ]);

        
    }

    public function logout() {
        Session::flush();
        return redirect()->route('user.index');
    }
}
