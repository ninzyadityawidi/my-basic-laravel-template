<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use App\Models\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    public function __construct()
    {
        
        $this->middleware(function ($request, $next){
            if(!hasPermission("SETTING [PERMISSION]")) {
                return redirect()->route('forbidden');
            }
            return $next($request);
        });

    }
    public function index() {
        $permissions = Permission::getPermission();
        return view("permission.index", compact("permissions"));
    }

    public function find(Request $request) {
        $module_id = $request->id;
        $permission = Permission::getPermission($module_id);
        return response()->json([
            "success" => true,
            "data"=> $permission
        ]);
    }

    public function getRole(Request $request) {
        $roles = Role::all();
        return response()->json($roles);
    }

    public function store(Request $request) {
        $module_id = $request->id;
        $role_can_access = $request->role_can_access;

        // dd($role_can_access);

        Permission::updateData($module_id,$role_can_access);

        return response()->json([
            'success'    => true,
            'statusCode' => 200,
            'msg'        => 'success',
            'callback'   => url('permission')
        ]);

    }
}
