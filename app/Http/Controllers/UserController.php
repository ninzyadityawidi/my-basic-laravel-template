<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Department;
use App\Models\Role;
use App\Models\Team;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next){
            if(!hasPermission("MASTER [USER]")) {
                return redirect()->route('forbidden');
            }
            return $next($request);
        });
    }
    public function index() {
        $users      = User::getUser();
        //$companies  = Company::all();
        $roles      = Role::all();
        //$departments= Department::all(); 
        //return view("user.index",compact("users","companies","roles","departments"));
        return view("user.index",compact("users","roles"));
    }

    public function findDepartmentByCompanyID(Request $request) {
        $id = $request->id;
        $department = Department::findDepartmentByCompanyID($id);
        return response()->json($department);
    }

    public function findTeamByDepartmentID(Request $request) {
        $id = $request->id;
        $team = Team::findTeamByDepartmentID($id);
        return response()->json($team);
    }

    public function find(Request $request) {
        $user = User::getUserByID($request->id);
        if($user) {
            return response([
                'success' => true,
                'data'    => $user
            ]);
        }

        return response([
            'success' => false,
            'msg'     => 'Data not found'
        ]);
    }

    public function store(Request $request) {
        $fullname      = $request->input("fullname");
        $role_id       = $request->input("role_id");
        $email         = $request->input("email");
        $password      = Hash::make('12345678'); //password default

        //validation header
        $rules = array(
            'fullname'   => 'required',
            'role_id'    => 'required',
            'email'      => array(
                'required',
                Rule::unique('users', 'email')
            ),
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }

        $user                = new User();
        $user->fullname      = $fullname;
        $user->role_id       = $role_id;
        $user->email         = $email;
        $user->password      = $password;
        $user->save();

        return response([
            'success'    => true,
            'msg'        => 'success', 
            'validation' => true,
            'callback'   => url('user'),                                                                                                                            
        ]);

    }

    public function update(Request $request) {
        $id            = $request->input("id");
        $fullname      = $request->input("fullname");
        $role_id       = $request->input("role_id");
        $email         = $request->input("email");

        //validation header
        $rules = array(
            'fullname'   => 'required',
            'role_id'    => 'required',
            'email'      => array(
                'required',
                Rule::unique('users', 'email')->ignore($id)
            ),
        );
        $validator = Validator::make($request->all(),$rules);
 
        if ($validator->fails()) {
            return response([
                'success'    => false,
                'msg'        => $validator->getMessageBag()->toArray(), 
                'validation' => true                                                                                                                                  
            ]);
        }
        $user  = User::where('id', $id)->update([
            'fullname' => $fullname,
            'role_id'  => $role_id,
            'email'    => $email,
        ]);
        if($user) {
            return response([
                'success'    => true,
                'msg'        => 'success',
                'callback'   => url('user'),                                                                                                                            
            ]);
        }
        
        return response([
            'success'    => false,
            'msg'        => 'Failed, info your IT Development',
            // 'callback'   => url('user'),                                                                                                                            
        ]);

    }

    public function resetPassword(Request $request) {
        $id   = $request->input("id");
        $user = User::find($id);
        if($user) {
            $user->update([
                'password' => Hash::make('12345678'),
            ]);

            return response([
                'success'    => true,
                'msg'        => 'success',
                'callback'   => url('user'),                                                                                                                            
            ]);
        }
    }

    public function changePassword(Request $request) {
        $id       = 6;
        $new_password = $request->input('new_password');
        $old_password = $request->input('old_password');
        $user = User::find($id);
        if($user) {
            $validation = Hash::check($old_password, $user->password);


            if($validation) {

                $user->update([
                    'password' => Hash::make($new_password),
                ]);
    
                return response([
                    'success'    => true,
                    'msg'        => 'success',
                    'callback'   => url('user'),                                                                                                                            
                ]);
            }

            return response([
                'success'=> false,
                'msg' => 'password failed',
            ]);
        }
    }

    public function delete(Request $request) {
        $id = $request->input('id');
        $user = User::find($id);
        if($user) {
            $user->delete();
            return response([
                'success'    => true,
                'msg'        => 'success', 
                'validation' => true,
                'callback'   => url('user'),                                                                                                                            
            ]);
        } else {
            return response([
                'success'    => false,
                'msg'        => 'failed, data not found',                                                                                                                               
            ]);
        }

    }
}
