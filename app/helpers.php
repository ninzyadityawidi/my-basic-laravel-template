<?php

if (! function_exists('show_date')) {
    function show_date() {
        return date('Y-m-d H:I');
    }
}

if (! function_exists('fullname')) {
    function fullname() {
        return session()->get('fullname');
    }
}

if (! function_exists('company_id')) {
    function company_id() {
        return session()->get('company_id');
    }
}

if (! function_exists('company_name')) {
    function company_name() {
        return session()->get('company_name');
    }
}


if (! function_exists('department_id')) {
    function department_id() {
        return session()->get('department_id');
    }
}

if (! function_exists('department_name')) {
    function department_name() {
        return session()->get('department_name');
    }
}

if (! function_exists('team_id')) {
    function team_id() {
        return session()->get('team_id');
    }
}

if (! function_exists('team_name')) {
    function team_name() {
        return session()->get('team_name');
    }
}

if (! function_exists('role_id')) {
    function role_id() {
        return session()->get('role_id');
    }
}

if (! function_exists('role_name')) {
    function role_name() {
        return session()->get('role_name');
    }
}

if (! function_exists('email')) {
    function email() {
        return session()->get('email');
    }
}

if (! function_exists('module')) {
    function module() {
        return session()->get('module');
    }
}

if (! function_exists('hasPermission')) {
    function hasPermission($module) {
        return (in_array($module, session()->get('module'))) ? true : false;
    }
}