<?php

use App\Http\Controllers\Authentication;
use App\Http\Controllers\CompanyController;
use App\Http\Controllers\DepartmentController;
use App\Http\Controllers\FatFull\Dashboard as DashboardFatFull;
use App\Http\Controllers\ModuleController;
use App\Http\Controllers\PermissionController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\TeamController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\TransactionController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/attex/starter', function(){
    return view('attex_starter');
});

Route::get('/', function () {
    if(is_null(session()->get('id'))) {
        return view('auth.login_attex');
    } else {
        return redirect()->route('user.index');
    }
})->name('login');

Route::get('/logout',[Authentication::class,'logout']);
Route::post('auth/login/validation',[Authentication::class,'loginValidation'])->name('loginValidation');
Route::get('/forbbiden',function(){
    return view('layouts.attex.403_forbidden');
})->name('forbidden');

Route::middleware(['cauth','web'])->group(function () {

    Route::get('dashboard', function(){
        return view('dashboard');
    })->name('dashboard');
    //crud user
    Route::get('/user',[UserController::class,'index'])->name('user.index');
    Route::get('/user/find',[UserController::class,'find']);
    Route::get('/user/find/department',[UserController::class,'findDepartmentByCompanyID']);
    Route::get('/user/find/team',[UserController::class,'findTeamByDepartmentID']);
    Route::post('/user',[UserController::class,'store']);
    Route::post('/user/update',[UserController::class,'update']);
    Route::get('/user/reset-password',[UserController::class,'resetPassword'])->name('reset.password');
    Route::post('/user/change-password',[UserController::class,'changePassword'])->name('change.password');
    Route::delete('/user',[UserController::class,'delete']);
    
    //crud module
    Route::get('/module',[ModuleController::class,'index'])->name('module.index');
    Route::get('/module/find',[ModuleController::class,'find']);
    Route::post('/module',[ModuleController::class,'store']);
    Route::post('/module/update',[ModuleController::class,'update']);
    Route::delete('/module',[ModuleController::class,'delete']);

    //crud permission
    Route::get('/permission',[PermissionController::class,'index'])->name('permission.index');
    Route::get('/permission/find',[PermissionController::class,'find']);
    Route::post('/permission',[PermissionController::class,'store']);
    Route::post('/permission/update',[PermissionController::class,'update']);
    Route::delete('/permission',[PermissionController::class,'delete']);
    Route::get('/permission/getRole',[PermissionController::class,'getRole']);

    //crud profile
    Route::get('/profile',[ProfileController::class,'index'])->name('profile.index');

    //crud role
    Route::get('/role',[RoleController::class, 'index'])->name('role.index');
    Route::get('/role/find',[RoleController::class,'find']);
    Route::post('/role',[RoleController::class,'store']);
    Route::post('/role/update',[RoleController::class,'update']);
    Route::delete('/role',[RoleController::class,'delete']);

});