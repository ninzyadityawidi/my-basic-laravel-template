<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('users')->insert([
            [
                'id' => '1',
                'fullname' => 'Super Administrator',
                'email' => 'superadmin@mail.com',
                'password' => '$2y$12$sJkwkw94oUPOKzYpgmqU9uKmUQbka6DirMWCNBl2Qpv.Sa.ZXGnL2',
                'role_id' => '1',
                'created_at' => '2023-11-26 05:11:54',
                'updated_at' => '2023-11-26 05:11:54',
                'deleted_at' => null,
            ],
            [
                'id' => '2',
                'fullname' => 'Administrator',
                'email' => 'admin@mail.com',
                'password' => '$2y$12$sJkwkw94oUPOKzYpgmqU9uKmUQbka6DirMWCNBl2Qpv.Sa.ZXGnL2',
                'role_id' => '2',
                'created_at' => '2023-11-26 05:11:54',
                'updated_at' => '2023-11-26 05:11:54',
                'deleted_at' => null,
            ]
        ]);
    }
}
