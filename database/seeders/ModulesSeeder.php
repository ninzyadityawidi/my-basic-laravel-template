<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ModulesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('modules')->insert([
            [
                'id' => '1',
                'name' => 'MASTER',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
            [
                'id' => '2',
                'name' => 'MASTER [ROLE]',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
            [
                'id' => '3',
                'name' => 'MASTER [USER]',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
            [
                'id' => '4',
                'name' => 'SETTING',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
            [
                'id' => '5',
                'name' => 'SETTING [MODULE]',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
            [
                'id' => '6',
                'name' => 'SETTING [PERMISSION]',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
        ]);
    }
}
