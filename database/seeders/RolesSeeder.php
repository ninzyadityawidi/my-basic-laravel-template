<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('roles')->insert([
            [
                'id' => '1',
                'name' => 'Superadmin',
                'created_at' => '2023-11-26 05:11:54',
                'updated_at' => '2023-11-26 05:11:54',
                'deleted_at' => null,
            ],
            [
                'id' => '2',
                'name' => 'Administrator',
                'created_at' => '2023-11-26 05:11:54',
                'updated_at' => '2023-11-26 05:11:54',
                'deleted_at' => null,
            ],
            [
                'id' => '3',
                'name' => 'User',
                'created_at' => '2023-11-26 05:11:54',
                'updated_at' => '2023-11-26 05:11:54',
                'deleted_at' => null,
            ]
        ]);
    }
}
