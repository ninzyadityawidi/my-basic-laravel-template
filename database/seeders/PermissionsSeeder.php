<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('permissions')->insert([
            [
                'id' => '1',
                'module_id' => '1',
                'role_id' => '1',
                'user_id' => '1',
                'is_allow' => '1',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
            [
                'id' => '2',
                'module_id' => '2',
                'role_id' => '1',
                'user_id' => '1',
                'is_allow' => '1',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
            [
                'id' => '3',
                'module_id' => '3',
                'role_id' => '1',
                'user_id' => '1',
                'is_allow' => '1',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
            [
                'id' => '4',
                'module_id' => '4',
                'role_id' => '1',
                'user_id' => '1',
                'is_allow' => '1',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
            [
                'id' => '5',
                'module_id' => '5',
                'role_id' => '1',
                'user_id' => '1',
                'is_allow' => '1',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
            [
                'id' => '6',
                'module_id' => '6',
                'role_id' => '1',
                'user_id' => '1',
                'is_allow' => '1',
                'created_at' => '2023-11-14 00:58:14',
                'updated_at' => '2023-11-14 00:58:14',
                'deleted_at' => null,
            ],
        ]);
    }
}
