$('.form-post').on('submit', function (e) {
    e.preventDefault();
    $.ajax({
        url: $(this).attr('data-uri'),
        type: 'POST',
        dataType: 'JSON',
        data: $(this).serialize(),
        beforeSend: function () {
            $('.btn-form').prop('disabled', true);
        },
        success: function (obj) {
            $('.btn-form').prop('disabled', false);
            if (obj.success) {
                toastr.success(obj.msg);
                if (obj.callback != undefined) {
                    window.location.href = obj.callback;
                }
            } else {
                if (obj.validation != undefined) {
                    $.each(obj.msg, function (key, value) {
                        toastr.error(value);
                    });
                } else {
                    toastr.error(obj.msg);
                }

            }
        },
        complete: function () {
            $('.btn-form').prop('disabled', false);
        }
    })
});

$('.form-multipart').on('submit', function (e) {
    e.preventDefault();
    let formData = new FormData($(this)[0]);
    $.ajax({
        url: $(this).attr('data-uri'),
        type: 'POST',
        dataType: 'JSON',
        data: formData,
        contentType: false,
        processData: false,
        beforeSend: function () {
            $('.btn-form').prop('disabled', true);
        },
        success: function (obj) {
            $('.btn-form').prop('disabled', false);
            if (obj.success) {
                toastr.success(obj.msg);
                if (obj.callback != undefined) {
                    window.location.href = obj.callback;
                }
            } else {
                if (obj.validation != undefined) {
                    $.each(obj.msg, function (key, value) {
                        toastr.error(value);
                    });
                } else {
                    toastr.error(obj.msg);
                }

            }
        },
        complete: function () {
            $('.btn-form').prop('disabled', false);
        }
    })
});


$(document).on('click', '.delete', function () {
    let id  = $(this).attr('data-id');
    let uri = $(this).attr('data-uri');

    Swal.fire({
        title: "Do you want to delete the item?",
        showDenyButton: true,
        showCancelButton: true,
        confirmButtonText: "Delete",
        denyButtonText: `No`,
        icon: "question"
    }).then((result) => {
        /* Read more about isConfirmed, isDenied below */
        if (result.isConfirmed) {
            $.ajax({
                url: uri,
                data: {
                    id:id,
                    "_token": token,
                },
                type: 'DELETE',
                dataType: 'JSON',
                beforeSend: function () {
                    $('.btn-form').prop('disabled', true);
                },
                success: function (obj) {
                    if (obj.success) {
                        toastr.success(obj.msg);
                        if (obj.callback != undefined) {
                            window.location.href = obj.callback;
                        }
                    } else {
                        if (obj.validation != undefined) {
                            $.each(obj.msg, function (key, value) {
                                toastr.error(value);
                            });
                        } else {
                            toastr.error(obj.msg);
                        }
        
                    }
                },
                complete: function () {
                    $('.btn-form').prop('disabled', false);
                }
            })
        } else if (result.isDenied) {
            Swal.fire("Changes are not saved", "", "info");
        }
    });
});

$('.mclose').on('click', function () {
    $('.modal').modal('hide');
});
