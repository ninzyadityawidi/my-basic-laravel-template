@extends('layouts.attex.main')


@section('content')

<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <form class="d-flex">
                            <div class="input-group">
                                <input type="text" class="form-control shadow border-0" id="dash-daterange">
                                <span class="input-group-text bg-primary border-primary text-white">
                                    <i class="ri-calendar-todo-fill fs-13"></i>
                                </span>
                            </div>
                            <a href="javascript: void(0);" class="btn btn-primary ms-2">
                                <i class="ri-refresh-line"></i>
                            </a>
                        </form>
                    </div>
                    <h4 class="page-title">Dashboard</h4>
                </div>
            </div>
        </div>

        <div class="row row-cols-1 row-cols-xxl-5 row-cols-lg-3 row-cols-md-2">
            <x-card-dashboard label="submitted" value="54,214" colorPrecentage="success" precentage="2.66%"
                colorChart="#47ad77,#e3e9ee" />

            <x-card-dashboard label="pickup" value="7,543" colorPrecentage="danger" precentage="1.08%"
                colorChart="#3e60d5,#e3e9ee" />

            <x-card-dashboard label="ondelivery" value="9,254" colorPrecentage="danger" precentage="7.00%"
                colorChart="#16a7e9,#e3e9ee" />

            <x-card-dashboard label="delivered" value="20.6" colorPrecentage="danger" precentage="7.00%"
                colorChart="#ffc35a,#e3e9ee" />

            <x-card-dashboard label="finished" value="9.62%" colorPrecentage="success" precentage="3.07%"
                colorChart="#f15776,#e3e9ee" />

        </div> <!-- end row -->

        <div class="row">
            <div class="col-xl-8 col-md-8 col-sm-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">Lead Time Performance</h4>
                        <div dir="ltr">
                            <div id="basic-column" class="apex-charts" data-colors="#3e60d5,#47ad77,#fa5c7c"></div>
                        </div>
                    </div>
                    <!-- end card body-->
                </div>
                <!-- end card -->
            </div>
            <div class="col-xl-4 col-md-4 col-sm-12">
                <div class="card">
                    <div class="d-flex card-header justify-content-between align-items-center">
                        <h4 class="header-title">Cost Insurance</h4>
                        <div class="dropdown">
                            <a href="#" class="dropdown-toggle arrow-none card-drop" data-bs-toggle="dropdown"
                                aria-expanded="false">
                                <i class="ri-more-2-fill"></i>
                            </a>
                        </div>
                    </div>

                    <div class="card-body p-0 text-center" style="min-height: 300px; display: flex; justify-content: center;align-items: center;">
                        <h2>RP. 25.000.000</h2>
                    </div> <!-- end card-body-->
                </div> <!-- end card-->
            </div> <!-- end col-->
        </div>

    </div> <!-- container -->

</div> <!-- content -->

@endsection



@section('additionalCss')
<link rel="stylesheet" href="{{ asset('attex/vendor/daterangepicker/daterangepicker.css') }}">

<!-- Vector Map css -->
<link rel="stylesheet" href="{{ asset('attex/vendor/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.css') }}">
@endsection

@section('additionalJs')
<!-- Daterangepicker js -->
<script src="{{ asset('attex/vendor/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('attex/vendor/daterangepicker/daterangepicker.js') }}"></script>

<!-- Apex Charts js -->
<script src="{{ asset('attex/vendor/apexcharts/apexcharts.min.js') }}"></script>

<!-- Vector Map js -->
<script src="{{ asset('attex/vendor/admin-resources/jquery.vectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('attex/vendor/admin-resources/jquery.vectormap/maps/jquery-jvectormap-world-mill-en.js') }}">
</script>

{{-- chart card --}}
<script>
    !(function (r) {
        "use strict";

        function e() {
            (this.$body = r("body")), (this.charts = []);
        }
        (e.prototype.initCharts = function () {
            window.Apex = {
                chart: {
                    parentHeightOffset: 0,
                    toolbar: {
                        show: !1
                    }
                },
                grid: {
                    padding: {
                        left: 0,
                        right: 0
                    }
                },
                colors: ["#3e60d5", "#47ad77", "#fa5c7c", "#ffbc00"],
            };
            var e = ["#3e60d5", "#47ad77"],
                //total wo
                t = r("#widget-submitted").data("colors"),
                a = {
                    chart: {
                        height: 72,
                        width: 72,
                        type: "donut"
                    },
                    legend: {
                        show: !1
                    },
                    plotOptions: {
                        pie: {
                            donut: {
                                size: "80%"
                            }
                        }
                    },
                    stroke: {
                        colors: ["transparent"]
                    },
                    series: [58, 42],
                    dataLabels: {
                        enabled: !1
                    },
                    colors: (e = t ? t.split(",") : e),
                },
                e =
                (new ApexCharts(
                        document.querySelector("#widget-submitted"),
                        a
                    ).render(),
                    ["#3e60d5", "#47ad77"]),
                a = {
                    chart: {
                        height: 72,
                        width: 72,
                        type: "donut"
                    },
                    legend: {
                        show: !1
                    },
                    stroke: {
                        colors: ["transparent"]
                    },
                    plotOptions: {
                        pie: {
                            donut: {
                                size: "80%"
                            }
                        }
                    },
                    series: [34, 66],
                    dataLabels: {
                        enabled: !1
                    },
                    colors: (e = (t = r("#widget-pickup").data("colors")) ?
                        t.split(",") :
                        e),
                },
                e =
                (new ApexCharts(document.querySelector("#widget-pickup"), a).render(),
                    ["#3e60d5", "#47ad77"]),
                a = {
                    chart: {
                        height: 72,
                        width: 72,
                        type: "donut"
                    },
                    legend: {
                        show: !1
                    },
                    stroke: {
                        colors: ["transparent"]
                    },
                    plotOptions: {
                        pie: {
                            donut: {
                                size: "80%"
                            }
                        }
                    },
                    series: [87, 13],
                    dataLabels: {
                        enabled: !1
                    },
                    colors: (e = (t = r("#widget-ondelivery").data("colors")) ?
                        t.split(",") :
                        e),
                },
                e =
                (new ApexCharts(document.querySelector("#widget-ondelivery"), a).render(),
                    ["#3e60d5", "#47ad77"]),
                a = {
                    chart: {
                        height: 72,
                        width: 72,
                        type: "donut"
                    },
                    legend: {
                        show: !1
                    },
                    stroke: {
                        colors: ["transparent"]
                    },
                    plotOptions: {
                        pie: {
                            donut: {
                                size: "80%"
                            }
                        }
                    },
                    series: [45, 55],
                    dataLabels: {
                        enabled: !1
                    },
                    colors: (e = (t = r("#widget-delivered").data("colors")) ?
                        t.split(",") :
                        e),
                },
                e =
                (new ApexCharts(document.querySelector("#widget-delivered"), a).render(),
                    ["#3e60d5", "#47ad77"]),
                a = {
                    chart: {
                        height: 72,
                        width: 72,
                        type: "donut"
                    },
                    legend: {
                        show: !1
                    },
                    stroke: {
                        colors: ["transparent"]
                    },
                    plotOptions: {
                        pie: {
                            donut: {
                                size: "80%"
                            }
                        }
                    },
                    series: [23, 68],
                    dataLabels: {
                        enabled: !1
                    },
                    colors: (e = (t = r("#widget-finish").data("colors")) ?
                        t.split(",") :
                        e),
                },
                e =
                (new ApexCharts(
                        document.querySelector("#widget-finish"),
                        a
                    ).render(),
                    ["#3e60d5", "#47ad77", "#fa5c7c", "#ffbc00"]),
                a = {
                    chart: {
                        height: 286,
                        type: "radialBar"
                    },
                    plotOptions: {
                        radialBar: {
                            startAngle: -135,
                            endAngle: 135,
                            dataLabels: {
                                name: {
                                    fontSize: "14px",
                                    color: void 0,
                                    offsetY: 100
                                },
                                value: {
                                    offsetY: 55,
                                    fontSize: "24px",
                                    color: void 0,
                                    formatter: function (e) {
                                        return e + "%";
                                    },
                                },
                            },
                            track: {
                                background: "rgba(170,184,197, 0.2)",
                                margin: 0
                            },
                        },
                    },
                    fill: {
                        gradient: {
                            enabled: !0,
                            shade: "dark",
                            shadeIntensity: 0.2,
                            inverseColors: !1,
                            opacityFrom: 1,
                            opacityTo: 1,
                            stops: [0, 50, 65, 91],
                        },
                    },
                    stroke: {
                        dashArray: 4
                    },
                    colors: (e = (t = r("#average-sales").data("colors")) ?
                        t.split(",") :
                        e),
                    series: [67],
                    labels: ["Returning Customer"],
                    responsive: [{
                        breakpoint: 380,
                        options: {
                            chart: {
                                height: 180
                            }
                        }
                    }],
                    grid: {
                        padding: {
                            top: 0,
                            right: 0,
                            bottom: 0,
                            left: 0
                        }
                    },
                },
                e =
                (new ApexCharts(document.querySelector("#average-sales"), a).render(),
                    ["#3e60d5", "#47ad77", "#fa5c7c", "#ffbc00"]);
        }),
        (e.prototype.init = function () {
            this.initCharts();
        }),
        (r.Dashboard = new e()),
        (r.Dashboard.Constructor = e);
    })(window.jQuery),
    (function (t) {
        "use strict";
        t(document).ready(function (e) {
            t.Dashboard.init();
        });
    })(window.jQuery);

</script>

{{-- chart column --}}
<script>
    var colors = ["#3e60d5", "#47ad77", "#fa5c7c"],
        dataColors = $("#basic-column").data("colors"),
        options = {
            chart: {
                height: 396,
                type: "bar",
                toolbar: {
                    show: !1
                }
            },
            plotOptions: {
                bar: {
                    horizontal: !1,
                    endingShape: "rounded",
                    columnWidth: "55%"
                }
            },
            dataLabels: {
                enabled: !1
            },
            stroke: {
                show: !0,
                width: 2,
                colors: ["transparent"]
            },
            colors: (colors = dataColors ? dataColors.split(",") : colors),
            series: [{
                    name: "Udara",
                    data: [44, 55, 57, 56, 61, 58, 63, 60, 66]
                },
                {
                    name: "Darat",
                    data: [76, 85, 101, 98, 87, 105, 91, 114, 94]
                },
                {
                    name: "Laut",
                    data: [35, 41, 36, 26, 45, 48, 52, 53, 41]
                },
            ],
            xaxis: {
                categories: ["Overdue 1 day", "Overdue 2 day", "Overdue 3 day", "Overdue 4 day", "Overdue 5 day", "Overdue 6 day", "Overdue 7 day", "Overdue 8 day", "Overdue 9 day"]
            },
            legend: {
                offsetY: 7
            },
            yaxis: {
                title: {
                    text: "$ (thousands)"
                }
            },
            fill: {
                opacity: 1
            },
            grid: {
                row: {
                    colors: ["transparent", "transparent"],
                    opacity: 0.2
                },
                borderColor: "#f1f3fa",
                padding: {
                    bottom: 5
                }
            },
            tooltip: {
                y: {
                    formatter: function (t) {
                        return "$ " + t + " thousands";
                    },
                },
            },
        },
        chart = new ApexCharts(document.querySelector("#basic-column"), options),
        colors = (chart.render(), ["#3e60d5"]);

</script>

@endsection