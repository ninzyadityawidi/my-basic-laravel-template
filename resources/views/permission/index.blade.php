@extends('layouts.attex.main')

@section('title', 'Role')


@section('content')
<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item active">Permission</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Permission</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">List Permission</h4>
                        <p class="text-muted fs-14">
                            All data Permission
                        </p>
                        <div class="table-responsive">
                            <table class="table table-centered table-striped datatable dt-responsive nowrap "
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Module Name</th>
                                        <th>Role Can Accsess</th>
                                        <th style="width: 120px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($permissions as $permission)
                                    <tr>
                                        <td>{{ $permission['module_name'] }}</td>
                                        <td>
                                            @if (is_array($permission['role_can_access']) &&
                                            isset($permission['role_can_access']['role_name']))
                                            @for ($rn = 0; $rn<count($permission['role_can_access']['role_name']);$rn++)
                                                <span class="badge bg-primary">
                                                {{ $permission['role_can_access']['role_name'][$rn] }}</span>

                                                @endfor
                                                @endif
                                        </td>
                                        <td>
                                            <a class="me-3 text-primary update" title="Edit"
                                                data-id="{{ $permission['module_id'] }}"
                                                data-uri="{{ url('permission/find') }}"><i
                                                    class="mdi mdi-pencil font-size-18"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- container -->

</div> <!-- content -->

<x-modals modalId="mUpdataPermission" title="Update Permission" size="">
    <form data-uri="{{ url('permission') }}" class="form-post">
        @csrf
        <div class="modal-body">
            <div class="row">
                <input type="hidden" name="id" id="uid" class="form-control">
                <x-input label="Module Name" grid="col-md-12">
                    <input type="text" name="module_name" id="umodule_name" class="form-control" disabled>
                </x-input>
                <x-input label="Role Can Accsess" grid="col-md-12">
                    <select name="role_can_access[]" id="urole_can_access" class="select2-updPermission form-control"
                        multiple>

                    </select>
                </x-input>
            </div>
        </div>
        <div class="modal-footer">
            <x-button type="button" class="btn btn-light waves-effect mclose" text="Close" />
            <x-button type="submit" class="btn btn-primary waves-effect waves-light" text="Save" />
        </div>
    </form>
</x-modals>
@endsection


@section('additionalCss')
@include('layouts.attex.plugins.datatables.css.datatables')

<link href="{{ asset('attex/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('additionalJs')
@include('layouts.attex.plugins.datatables.js.datatables')

<script src="{{ asset('attex/libs/sweetalert2/sweetalert2.min.js') }}"></script>

<script>
    $('.select2-updPermission').select2({
        dropdownParent: $('#mUpdataPermission')
    });

    $('#addPermission').on('click', function () {
        $('#mAddPermission').modal('show');
    });

    $(document).on('click', '.update', async function () {
        let id = $(this).attr('data-id');
        let uri = $(this).attr('data-uri');

        $('#urole_can_access').empty();

        let dataRole = await getRole();

        $.each(dataRole, (i, val) => {
            let optionContent = `<option value='${val.id}'>${val.name}</option>`;
            $('#urole_can_access').append(optionContent);
        });

        $.ajax({
            url: uri,
            data: {
                id: id,
            },
            type: 'GET',
            dataType: 'JSON',
            success: function (obj) {
                if (obj.success) {
                    $('#uid').val(obj.data[0].module_id);
                    $('#umodule_name').val(obj.data[0].module_name);

                    //role_can_accsess

                    if (Object.keys(obj.data[0].role_can_access).length > 0) {
                        let select2Instance = $('#urole_can_access').data('select2');
                        let roleName = obj.data[0].role_can_access.role_name;
                        let roleID = obj.data[0].role_can_access.role_id;

                        let setUpRoleCanAccess = [];
                        for (let rd = 0; rd < roleName.length; rd++) {
                            // array push
                            setUpRoleCanAccess.push({
                                id: roleID[rd],
                                text: roleName[rd]
                            });
                        }

                        setSelect2Values(setUpRoleCanAccess, '#urole_can_access');
                    }
                    $('#mUpdataPermission').modal('show');
                } else {

                }
            }
        });





    });


    function setSelect2Values(data, idSelect2 = "") {
        // Get the select2 instance
        var select2Instance = $(idSelect2);

        // Clear current selections

        // Set the values in Select2
        select2Instance.val(data.map(function (item) {
            return item.id;
        })).trigger('change');
    }


    function getRole() {
        return new Promise(function (resolve, reject) {
            $.ajax({
                url: `{{ url('permission/getRole') }}`,
                type: 'GET',
                dataType: 'JSON',
                success: function (data) {
                    resolve(data);
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    reject(errorThrown);
                }
            });
        });
    }

</script>
@endsection
