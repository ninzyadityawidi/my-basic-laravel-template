@extends('layouts.main')

@section('title', 'Blank Page')


@section('page-title')
<div class="col-sm-6">
    <div class="page-title">
        <h4>Blank Page</h4>
        <ol class="breadcrumb m-0">
            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
            <li class="breadcrumb-item"><a href="javascript: void(0);">Pages</a></li>
            <li class="breadcrumb-item active">Blank Page</li>
        </ol>
    </div>
</div>
<div class="col-sm-6">
    <div class="float-end d-none d-sm-block">
        {{-- <a href="" class="btn btn-success">Add Widget</a> --}}
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card">
            <div class="card-body">
                <div class="p-4" style="height: 300px;">

                </div>

            </div>
        </div>
    </div>
</div>
@endsection
