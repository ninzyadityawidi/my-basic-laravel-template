<div>
    <!-- Very little is needed to make a happy life. - Marcus Aurelius -->
    <button type="{{ $type }}" class="{{ $class }}" id="{{ $id }}">{{ $text }}</button>
</div>