<div class="col">
    <div class="card">
        <div class="card-body">
            <div class="d-flex justify-content-between">
                <div class="flex-grow-1 overflow-hidden">
                    <h5 class="text-muted fw-normal mt-0" title="Number of Customers">{{ ucwords($label) }}</h5>
                    <h3 class="my-3">{{ $value }}</h3>
                    <p class="mb-0 text-muted text-truncate">
                        <span class="badge bg-{{ $colorPrecentage }} me-1"><i class="ri-arrow-up-line"></i> {{ $precentage }}</span>
                        <span>Since last month</span>
                    </p>
                </div>
                <div class="flex-shrink-0">
                    <div id="widget-{{ $label }}" class="apex-charts" data-colors="{{ $colorChart }}"></div>
                </div>
            </div>
        </div> <!-- end card-body-->
    </div> <!-- end card-->
</div> <!-- end col-->
