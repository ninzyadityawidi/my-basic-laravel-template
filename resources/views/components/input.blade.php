<div class="{{ $grid }}">
    <div class="mb-3">
        <label for="{{ strtolower($label) }}" class="form-label">{{ $label }}</label>
        {{ $slot }}
    </div>
</div>
