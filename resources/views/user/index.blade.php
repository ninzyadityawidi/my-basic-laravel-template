@extends('layouts.attex.main')

@section('title', 'User')

@section('content')

<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item active">User</li>
                        </ol>
                    </div>
                    <h4 class="page-title">User <button type="button" class="btn btn-success btn-sm ms-3" id="addUser">Add New</button></h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">List Users</h4>
                        <p class="text-muted fs-14">
                            All data user
                        </p>
                        <div class="table-responsive">
                            <table class="table table-striped table-centered datatable dt-responsive nowrap "
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Fullname</th>
                                        <th>Email</th>
                                        <th>Role</th>
                                        <th style="width: 120px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($users as $user)
                                    <tr>
                                        <td>{{ $user->fullname }}</td>
                                        <td>{{ $user->email }}</td>
                                        <td>{{ $user->role_name }}</td>
                                        <td>
                                            <a class="me-3 text-primary update" title="Edit" data-id="{{ $user->id }}"
                                                data-uri="{{ url('user/find') }}"><i
                                                    class="mdi mdi-pencil font-size-18"></i></a>
                                            <a class="me-3 text-warning reset" title="Change Password"
                                                onclick="actionQuestion('{{ $user->id }}','{{ route('reset.password') }}','Are you sure reset thid passowd ?','Reset')"><i
                                                    class="mdi mdi-key-change font-size-18"></i></a>
                                            <a class="text-danger delete" title="Delete" data-id="{{ $user->id }}"
                                                data-uri="{{ url('user') }}"><i
                                                    class="mdi mdi-trash-can font-size-18"></i></a>

                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- container -->

</div> <!-- content -->


<x-modals modalId="mAddUser" title="Add User" size="modal-lg">
    <form data-uri="{{ url('user') }}" class="form-post">
        @csrf
        <div class="modal-body">
            <div class="row">
                <x-input label="Fullname">
                    <input type="text" name="fullname" id="fullname" class="form-control" autocomplete="off">
                </x-input>
                {{-- <x-input label="Company">
                    <select name="company_id" id="company_id" class="form-control select2-addUser">
                        <option value="">Select Company</option>
                        @foreach ($companies as $company)
                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                        @endforeach
                    </select>
                </x-input> --}}
                <x-input label="Email">
                    <input type="email" name="email" id="email" class="form-control" autocomplete="off">
                </x-input>
                <x-input label="Role">
                    <select name="role_id" id="role_id" class="form-control select2-addUser">
                        @foreach ($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                </x-input>
                {{-- <x-input label="Department">
                    <select name="department_id" id="department_id" class="form-control select2-addUser">
                        
                    </select>
                </x-input>
                <x-input label="Teams">
                    <select name="team_id" id="team_id" class="form-control select2-addUser">
                        
                    </select>
                </x-input> --}}
            </div>
        </div>
        <div class="modal-footer">
            <x-button type="button" class="btn btn-light waves-effect mclose" text="Close" />
            <x-button type="submit" class="btn btn-primary waves-effect waves-light" text="Save" />
        </div>
    </form>
</x-modals>


<x-modals modalId="mUpdataUser" title="Update User" size="modal-lg">
    <form data-uri="{{ url('user/update') }}" class="form-post">
        @csrf
        <div class="modal-body">
            <div class="row">
                <input type="hidden" name="id" id="uid" class="form-control">
                <x-input label="Fullname">
                    <input type="text" name="fullname" id="ufullname" class="form-control">
                </x-input>
                {{-- <x-input label="Company">
                    <select name="company_id" id="ucompany_id" class="form-control select2-updUser">
                        @foreach ($companies as $company)
                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                        @endforeach
                    </select>
                </x-input> --}}
                <x-input label="Email">
                    <input type="email" name="email" id="uemail" class="form-control">
                </x-input>
                <x-input label="Role">
                    <select name="role_id" class="form-control select2-updUser">
                        @foreach ($roles as $role)
                        <option value="{{ $role->id }}">{{ $role->name }}</option>
                        @endforeach
                    </select>
                </x-input>
                {{-- <x-input label="Department">
                    <select name="department_id" id="udepartment_id" class="form-control select2-updUser">
                        
                    </select>
                </x-input>
                <x-input label="Teams">
                    <select name="team_id" id="uteam_id" class="form-control select2-updUser">
                        
                    </select>
                </x-input> --}}
            </div>
        </div>
        <div class="modal-footer">
            <x-button type="button" class="btn btn-light waves-effect mclose" text="Close" />
            <x-button type="submit" class="btn btn-primary waves-effect waves-light" text="Save" />
        </div>
    </form>
</x-modals>
@endsection


@section('additionalCss')
@include('layouts.attex.plugins.datatables.css.datatables')
<link href="{{ asset('attex/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('additionalJs')
@include('layouts.attex.plugins.datatables.js.datatables')
<script src="{{ asset('attex/libs/sweetalert2/sweetalert2.min.js') }}"></script>


<script>
    //select2 for modal add
    $('.select2-addUser').select2({
        dropdownParent: $("#mAddUser")
    });
    $('.select2-updUser').select2({
        dropdownParent: $('#mUpdataUser')
    });
    //end select2

    $('#addUser').on('click', function () {
        $('#mAddUser').modal('show');
    });

    // $('#company_id').change(function(){
    //     let id = $(this).val();
    //     $('#team_id').empty();
    //     getDepartment(id,'#department_id');
    // });

    // $('#ucompany_id').change(function(){
    //     let id = $(this).val();
    //     $('#uteam_id').empty();
    //     getDepartment(id,'#udepartment_id');
    // });

    // $('#department_id').change(function(){
    //     let id = $(this).val();
    //     getTeams(id,'#team_id');
    // });

    // $('#udepartment_id').change(function(){
    //     let id = $(this).val();
    //     getTeams(id,'#uteam_id');
    // });


    $(document).on('click', '.update', function () {
        let id = $(this).attr('data-id');
        let uri = $(this).attr('data-uri');

        $.ajax({
            url: uri,
            data: {
                id: id,
            },
            type: 'GET',
            dataType: 'JSON',
            success: function (obj) {
                if (obj.success) {
                    $('#uid').val(id);
                    $('#ufullname').val(obj.data.fullname);
                    $('#uemail').val(obj.data.email);
                    // $('#ucompany_id').val(obj.data.company_id);
                    $('#urole_id').val(obj.data.role_id);
                    //getDepartment(obj.data.department_id,'#udepartment_id',1);
                    //getTeams(obj.data.team_id,'#uteam_id',1);
                    $('#mUpdataUser').modal('show');
                } else {

                }
            }
        });
    });

</script>
@endsection
