@extends('layouts.attex.main')

@section('title', 'Department')


@section('content')

<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item active">Team</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Profile</h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-xl-4 col-lg-5">
                <div class="card text-center">
                    <div class="card-body">
                        <img src="{{ asset('images/user.png') }}" class="rounded-circle avatar-lg img-thumbnail"
                            alt="profile-image">

                        <h4 class="mb-1 mt-2">{{ fullname() }}</h4>
                        <p class="text-muted">{{ role_name() }}</p>


                        <div class="text-start mt-3">
                            <p class="text-muted mb-2"><strong>Full Name :</strong> <span
                                    class="ms-2">{{ fullname() }}</span></p>

                            <p class="text-muted mb-2"><strong>Mobile :</strong><span class="ms-2">(123)
                                    123 1234</span></p>

                            <p class="text-muted mb-2"><strong>Email :</strong> <span
                                    class="ms-2 ">{{ email() }}</span></p>

                            <p class="text-muted mb-1"><strong>Work In :</strong> <span
                                    class="ms-2">{{ company_name() }}</span></p>
                        </div>

                        <ul class="social-list list-inline mt-3 mb-0">
                            <li class="list-inline-item">
                                <a href="javascript: void(0);" class="social-list-item border-primary text-primary"><i
                                        class="ri-facebook-circle-fill"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript: void(0);" class="social-list-item border-danger text-danger"><i
                                        class="ri-google-fill"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript: void(0);" class="social-list-item border-info text-info"><i
                                        class="ri-twitter-fill"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a href="javascript: void(0);"
                                    class="social-list-item border-secondary text-secondary"><i
                                        class="ri-github-fill"></i></a>
                            </li>
                        </ul>
                    </div> <!-- end card-body -->
                </div> <!-- end card -->

            </div> <!-- end col-->

            <div class="col-xl-8 col-lg-7">

                <div class="card">
                    <div class="card-body">
                        <ul class="nav nav-pills bg-nav-pills nav-justified mb-3">
                            <li class="nav-item">
                                <a href="#aboutme" data-bs-toggle="tab" aria-expanded="false"
                                    class="nav-link rounded-start rounded-0 active">
                                    About
                                </a>
                            </li>
                            <li class="nav-item">
                                <a href="#settings" data-bs-toggle="tab" aria-expanded="false"
                                    class="nav-link rounded-end rounded-0">
                                    Settings
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane show active" id="aboutme">

                                <h5 class="text-uppercase mt-4"><i class="ri-macbook-line me-1"></i>
                                    Activity</h5>

                                <div class="timeline-alt pb-0">
                                    <div class="timeline-item">
                                        <i class="ri-record-circle-line text-bg-info timeline-icon"></i>
                                        <div class="timeline-item-info">
                                            <h5 class="mt-0 mb-1">Lead designer / Developer</h5>
                                            <p class="fs-14">websitename.com <span class="ms-2 fs-12">Year: 2015 -
                                                    18</span></p>
                                            <p class="text-muted mt-2 mb-0 pb-3">Everyone realizes why a new common
                                                language
                                                would be desirable: one could refuse to pay expensive translators.
                                                To achieve this, it would be necessary to have uniform grammar,
                                                pronunciation and more common words.</p>
                                        </div>
                                    </div>

                                    <div class="timeline-item">
                                        <i class="ri-record-circle-line text-bg-primary timeline-icon"></i>
                                        <div class="timeline-item-info">
                                            <h5 class="mt-0 mb-1">Senior Graphic Designer</h5>
                                            <p class="fs-14">Software Inc. <span class="ms-2 fs-12">Year: 2012 -
                                                    15</span></p>
                                            <p class="text-muted mt-2 mb-0 pb-3">If several languages coalesce, the
                                                grammar
                                                of the resulting language is more simple and regular than that of
                                                the individual languages. The new common language will be more
                                                simple and regular than the existing European languages.</p>

                                        </div>
                                    </div>

                                    <div class="timeline-item">
                                        <i class="ri-record-circle-line text-bg-info timeline-icon"></i>
                                        <div class="timeline-item-info">
                                            <h5 class="mt-0 mb-1">Graphic Designer</h5>
                                            <p class="fs-14">Coderthemes Design LLP <span class="ms-2 fs-12">Year: 2010
                                                    - 12</span></p>
                                            <p class="text-muted mt-2 mb-0 pb-2">The European languages are members of
                                                the same family. Their separate existence is a myth. For science
                                                music sport etc, Europe uses the same vocabulary. The languages
                                                only differ in their grammar their pronunciation.</p>
                                        </div>
                                    </div>

                                </div>
                                <!-- end timeline -->

                            </div> <!-- end tab-pane -->
                            <!-- end about me section content -->

                            <div class="tab-pane" id="settings">
                                <form>
                                    <h5 class="mb-4 text-uppercase"><i class="ri-contacts-book-2-line me-1"></i>
                                        Personal Info</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="fullname" class="form-label">Fullname</label>
                                                <input type="text" class="form-control" id="fullname" name="fullname"
                                                    placeholder="Enter Fullname" value="{{ session()->get('fullname') }}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="email" class="form-label">Email Address</label>
                                                <input type="email" class="form-control" id="email" name="email"
                                                    placeholder="Enter email" value="{{ session()->get('email') }}">
                                            </div>
                                        </div>
                                    </div> <!-- end row -->

                                    <h5 class="mb-3 text-uppercase bg-light p-2"><i class="ri-building-line me-1"></i>
                                        Company Info</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="company" class="form-label">Company Name</label>
                                                <input type="text" class="form-control" id="company"
                                                    placeholder="Enter Company"
                                                    value="{{ session()->get('company_name') }}" disabled>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="cwebsite" class="form-label">Website</label>
                                                <input type="text" class="form-control" id="cwebsite" disabled>
                                            </div>
                                        </div> <!-- end col -->
                                    </div> <!-- end row -->

                                    <div class="text-end">
                                        <button type="submit" class="btn btn-success mt-2"><i class="ri-save-line"></i>
                                            Save</button>
                                    </div>
                                </form>

                                <form action="">
                                    <h5 class="mb-4 text-uppercase"><i class="ri-contacts-book-2-line me-1"></i>
                                        Change Password</h5>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="old_password" class="form-label">Old Password</label>
                                                <input type="password" class="form-control" id="old_password" name="old_password"
                                                    placeholder="Enter Old Password">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="mb-3">
                                                <label for="new_password" class="form-label">New Password</label>
                                                <input type="password" class="form-control" id="New Password"
                                                    placeholder="Enter New Password">
                                            </div>
                                        </div>
                                    </div> <!-- end row -->
                                    <div class="text-end">
                                        <button type="submit" class="btn btn-success mt-2"><i class="ri-save-line"></i>
                                            Change Password</button>
                                    </div>
                                </form>
                            </div>
                            <!-- end settings content-->

                        </div> <!-- end tab-content -->
                    </div> <!-- end card body -->
                </div> <!-- end card -->
            </div> <!-- end col -->
        </div>

    </div> <!-- container -->

</div> <!-- content -->

@endsection
