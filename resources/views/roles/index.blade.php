@extends('layouts.attex.main')

@section('title', 'Role')


@section('content')
<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item active">Roles</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Roles <button type="button" class="btn btn-success btn-sm ms-3" id="addRole">Add New</button></h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">List Roles</h4>
                        <p class="text-muted fs-14">
                            All data Roles
                        </p>
                        <div class="table-responsive">
                            <table class="table table-centered table-striped datatable dt-responsive nowrap "
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Name</th>
                                        <th style="width: 120px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($roles as $role)
                                    <tr>
                                        <td>{{ $role->name }}</td>
                                        <td>
                                            <a class="me-3 text-primary update" title="Edit" data-id="{{ $role->id }}"
                                                data-uri="{{ url('role/find') }}"><i
                                                    class="mdi mdi-pencil font-size-18"></i></a>
                                            <a class="text-danger delete" title="Delete" data-id="{{ $role->id }}"
                                                data-uri="{{ url('role') }}"><i class="mdi mdi-trash-can font-size-18"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- container -->

</div> <!-- content -->

<x-modals modalId="mAddRole" title="Add Role" size="">
    <form data-uri="{{ url('role') }}" class="form-post">
        @csrf
        <div class="modal-body">
            <div class="row">
                <x-input label="Name" grid="col-md-12">
                    <input type="text" name="name" id="name" class="form-control">
                </x-input>
            </div>
        </div>
        <div class="modal-footer">
            <x-button type="button" class="btn btn-light waves-effect mclose" text="Close" />
            <x-button type="submit" class="btn btn-primary waves-effect waves-light" text="Save" />
        </div>
    </form>
</x-modals>

<x-modals modalId="mUpdateRole" title="Update Role" size="">
    <form data-uri="{{ url('role/update') }}" class="form-post">
        @csrf
        <div class="modal-body">
            <div class="row">
                <input type="hidden" name="id" id="uid" class="form-control">
                <x-input label="Name" grid="col-md-12">
                    <input type="text" name="name" id="uname" class="form-control">
                </x-input>
            </div>
        </div>
        <div class="modal-footer">
            <x-button type="button" class="btn btn-light waves-effect mclose" text="Close" />
            <x-button type="submit" class="btn btn-primary waves-effect waves-light" text="Save" />
        </div>
    </form>
</x-modals>
@endsection


@section('additionalCss')
@include('layouts.attex.plugins.datatables.css.datatables')

<link href="{{ asset('attex/libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />

@endsection

@section('additionalJs')
@include('layouts.attex.plugins.datatables.js.datatables')
<script src="{{ asset('attex/libs/sweetalert2/sweetalert2.min.js') }}"></script>

<script>
    $('#addRole').on('click', function () {
        $('#mAddRole').modal('show');
    });

    $(document).on('click', '.update', function () {
        let id = $(this).attr('data-id');
        let uri = $(this).attr('data-uri');

        $.ajax({
            url: uri,
            data: {
                id: id,
            },
            type: 'GET',
            dataType: 'JSON',
            success: function (obj) {
                if (obj.success) {
                    $('#uid').val(id);
                    $('#uname').val(obj.data.name);

                    $('#mUpdateRole').modal('show');
                } else {

                }
            }
        })
    });

</script>
@endsection
