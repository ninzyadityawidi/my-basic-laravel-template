@extends('layouts.attex.main')

@section('title', 'Department')

@section('content')

<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item active">Department</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Department <button type="button" class="btn btn-success btn-sm ms-3" id="addDepartment">Add New</button></h4>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-body">
                        <h4 class="header-title">List Department</h4>
                        <p class="text-muted fs-14">
                            All data department
                        </p>
                        <div class="table-responsive">
                            <table class="table table-centered table-striped datatable dt-responsive nowrap "
                                style="border-collapse: collapse; border-spacing: 0; width: 100%;">
                                <thead class="thead-light">
                                    <tr>
                                        <th>Name</th>
                                        <th>Company Name</th>
                                        <th style="width: 120px;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($departments as $department)
                                        <tr>
                                            <td>{{ $department->name }}</td>
                                            <td>{{ $department->company_name }}</td>
                                            <td>
                                                <a class="me-3 text-primary update" title="Edit" data-id="{{ $department->id }}"
                                                    data-uri="{{ url('department/find') }}"><i
                                                    class="mdi mdi-pencil font-size-18"></i></a>
                                                <a class="text-danger delete" title="Delete" data-id="{{ $department->id }}"
                                                    data-uri="{{ url('department') }}"><i class="mdi mdi-trash-can font-size-18"></i></a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div> <!-- container -->

</div> <!-- content -->


<x-modals modalId="mAddDepartment" title="Add Department" size="">
    <form data-uri="{{ url('department') }}" class="form-post">
        @csrf
        <div class="modal-body">
            <div class="row">
                <x-input label="Name" grid="col-md-12">
                    <input type="text" name="name" id="name" class="form-control">
                </x-input>
                <x-input label="Company" grid="col-md-12">
                    <select name="company" id="company" class="form-control">
                        @foreach ($companies as $company)
                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                        @endforeach
                    </select>
                </x-input>
            </div>
        </div>
        <div class="modal-footer">
            <x-button type="button" class="btn btn-light waves-effect mclose" text="Close" />
            <x-button type="submit" class="btn btn-primary waves-effect waves-light" text="Save" />
        </div>
    </form>
</x-modals>

<x-modals modalId="mUpdateDepartment" title="Update Department" size="">
    <form data-uri="{{ url('department/update') }}" class="form-post">
        @csrf
        <div class="modal-body">
            <div class="row">
                <input type="hidden" name="id" id="uid" class="form-control">
                <x-input label="Name" grid="col-md-12">
                    <input type="text" name="name" id="uname" class="form-control">
                </x-input>
                <x-input label="Company" grid="col-md-12">
                    <select name="company" id="ucompany" class="form-control">
                        @foreach ($companies as $company)
                        <option value="{{ $company->id }}">{{ $company->name }}</option>
                        @endforeach
                    </select>
                </x-input>
            </div>
        </div>
        <div class="modal-footer">
            <x-button type="button" class="btn btn-light waves-effect mclose" text="Close" />
            <x-button type="submit" class="btn btn-primary waves-effect waves-light" text="Save" />
        </div>
    </form>
</x-modals>
@endsection


@section('additionalCss')
@include('layouts.attex.plugins.datatables.css.datatables')

<link href="{{ asset('libs/bootstrap-datepicker/css/bootstrap-datepicker.min.css') }}" rel="stylesheet">
<link href="{{ asset('libs/spectrum-colorpicker2/spectrum.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css') }}" rel="stylesheet" />
<link href="{{ asset('libs/sweetalert2/sweetalert2.min.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('additionalJs')
@include('layouts.attex.plugins.datatables.js.datatables')
<!-- Form input -->
<script src="{{ asset('libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('libs/spectrum-colorpicker2/spectrum.min.js') }}"></script>
<script src="{{ asset('libs/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js') }}"></script>
<script src="{{ asset('libs/bootstrap-maxlength/bootstrap-maxlength.min.js') }}"></script>
<script src="{{ asset('js/pages/form-advanced.init.js') }}"></script>
<script src="{{ asset('libs/sweetalert2/sweetalert2.min.js') }}"></script>

<script>
    $('#addDepartment').on('click', function () {
        $('#mAddDepartment').modal('show');
    });

    $(document).on('click', '.update', function () {
        let id = $(this).attr('data-id');
        let uri = $(this).attr('data-uri');

        $.ajax({
            url: uri,
            data: {
                id:id,
            },
            type: 'GET',
            dataType: 'JSON',
            success: function(obj) {
                if(obj.success) {
                    $('#uid').val(id);
                    $('#uname').val(obj.data.name);

                    $('#mUpdateDepartment').modal('show');
                } else {

                }
            }
        })
    });

</script>
@endsection
