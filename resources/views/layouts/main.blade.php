<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title','Logistic Moratelindo')</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Logistic Mora Telematika Indonesia Tbk" name="description" />
    <meta content="Huda Alfarizi" name="author" />
    <!-- App favicon -->
    <link rel="shortcut icon" href="{{ asset('images/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('css/app.min.css') }}" id="app-style">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}" id="bootstrap-style">
    <link rel="stylesheet" href="{{ asset('css/icons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('libs/toastr/toastr.min.css') }}">

    @yield('additionalCss')

    <script>
        var token = "{{ csrf_token() }}";

    </script>

    @vite([
    'resources/js/global.js'
    ])

</head>

<body>
    <div id="preloader">
        <div id="status">
            <div class="spinner-chase">
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
                <div class="chase-dot"></div>
            </div>
        </div>
    </div>

    <!-- Begin page -->
    <div id="layout-wrapper">

        <!-- Include Header -->
        @include('layouts.partials.header')

        <!-- include Sidebar -->
        @include('layouts.partials.sidebar')

        <!-- Content -->
        <div class="main-content">
            <div class="page-content">

                <div class="page-title-box">
                    <div class="container-fluid">
                        <div class="row align-items-center">
                            @yield('page-title')


                        </div>
                    </div>
                </div>

                <div class="container-fluid">

                    <div class="page-content-wrapper">
                        @yield('content')
                    </div>


                </div> <!-- container-fluid -->

            </div>

            @include('layouts.partials.footer')
        </div>


        <x-modals modalId="mChangePassword" title="Change Password">
            <form data-uri="{{ route('change.password') }}" class="form-post">
                @csrf
                <div class="modal-body">
                    <div class="row">
                        <x-input label="Old password">
                            <input type="text" name="old_password" id="old_password" class="form-control">
                        </x-input>
                        <x-input label="New Password">
                            <input type="text" name="new_password" id="new_password" class="form-control">
                        </x-input>
                    </div>
                </div>
                <div class="modal-footer">
                    <x-button type="button" class="btn btn-light waves-effect mclose" text="Close" />
                    <x-button type="submit" class="btn btn-primary waves-effect waves-light" text="Save" />
                </div>
            </form>
        </x-modals>
    </div>

    @include('layouts.partials.right_sidebar')


    <div class="rightbar-overlay"></div>


    <!-- JAVASCRIPT -->
    <script src="{{ asset('libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('libs/metismenu/metisMenu.min.js') }}"></script>
    <script src="{{ asset('libs/simplebar/simplebar.min.js') }}"></script>
    <script src="{{ asset('libs/node-waves/waves.min.js') }}"></script>
    <script src="{{ asset('libs/toastr/toastr.min.js') }}"></script>

    @yield('additionalJs')

    <script src="{{ asset('js/app.js') }}"></script>

    <script>
        function actionQuestion(id, uri, textQuestion, textConfirm, type) {
            Swal.fire({
                title: textQuestion,
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: textConfirm,
                denyButtonText: `No`,
                icon: "question"
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        url: uri,
                        data: {
                            id: id,
                            "_token": token,
                        },
                        type: type,
                        dataType: 'JSON',
                        beforeSend: function () {
                            $('.btn-form').prop('disabled', true);
                        },
                        success: function (obj) {
                            if (obj.success) {
                                toastr.success(obj.msg);
                                if (obj.callback != undefined) {
                                    window.location.href = obj.callback;
                                }
                            } else {
                                if (obj.validation != undefined) {
                                    $.each(obj.msg, function (key, value) {
                                        toastr.error(value);
                                    });
                                } else {
                                    toastr.error(obj.msg);
                                }

                            }
                        },
                        complete: function () {
                            $('.btn-form').prop('disabled', false);
                        }
                    })
                } else if (result.isDenied) {
                    Swal.fire("Changes are not saved", "", "info");
                }
            });
        }

        $('.btnChangePassword').on('click', function(e){
            e.preventDefault();
            $('#mChangePassword').modal('show');
        });

    </script>
</body>

</html>
