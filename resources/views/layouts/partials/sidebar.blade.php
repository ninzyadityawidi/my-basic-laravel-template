<!-- ========== Left Sidebar Start ========== -->
<div class="vertical-menu">

    <div data-simplebar class="h-100">


        <div class="user-sidebar text-center">
            <div class="dropdown">
                <div class="user-img">
                    <img src="{{ asset('images/boss.png') }}" alt="" class="rounded-circle">
                    <span class="avatar-online bg-success"></span>
                </div>
                <div class="user-info">
                    <h5 class="mt-3 font-size-16 text-white">{{ session()->get('fullname') }}</h5>
                    <h6 class="mt-3 font-size-12 text-white">{{ session()->get('company_name') }}</h6>
                    <span class="font-size-13 text-white-50">{{ session()->get('role_name')  }}</span>
                </div>
            </div>
        </div>



        <!--- Sidemenu -->
        <div id="sidebar-menu">
            <!-- Left Menu Start -->
            <ul class="metismenu list-unstyled" id="side-menu">
                <li class="menu-title">Menu</li>

                <li>
                    <a href="index.html" class="waves-effect">
                        <i class="dripicons-home"></i>
                        <span>Dashboard</span>
                    </a>
                </li>

                <li>
                    <a href="javascript: void(0);" class="has-arrow waves-effect">
                        <i class="dripicons-cart"></i>
                        <span>Transaction</span>
                    </a>
                    <ul class="sub-menu" aria-expanded="false">
                        <li><a href="ecommerce-products.html">All</a></li>
                        <li><a href="ecommerce-orders.html">Open</a></li>
                        <li><a href="ecommerce-orders.html">Waiting Pickup</a></li>
                        <li><a href="ecommerce-orders.html">On Delivery</a></li>
                        <li><a href="ecommerce-orders.html">Delivered</a></li>
                    </ul>
                </li>

                <li class="menu-title">Master</li>
                <li>
                    <a href="{{ route('company.index') }}" class="waves-effect">
                        <i class="dripicons-network-3"></i>
                        <span>Company</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('department.index') }}" class="waves-effect">
                        <i class="dripicons-crosshair"></i>
                        <span>Department</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('role.index') }}" class="waves-effect">
                        <i class="dripicons-lock"></i>
                        <span>Role</span>
                    </a>
                </li>
                <li>
                    <a href="{{ route('user.index') }}" class="waves-effect">
                        <i class="dripicons-user"></i>
                        <span>User</span>
                    </a>
                </li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
</div>
<!-- Left Sidebar End -->