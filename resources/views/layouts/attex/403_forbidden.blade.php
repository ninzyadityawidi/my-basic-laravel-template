@extends('layouts.attex.main')


@section('content')

<div class="content">

    <!-- Start Content-->
    <div class="container-fluid">

        <div class="row">
            <div class="col-12">
                <div class="page-title-box">
                    <div class="page-title-right">
                        <ol class="breadcrumb m-0">
                            <li class="breadcrumb-item"><a href="javascript: void(0);">Home</a></li>
                            <li class="breadcrumb-item active">403</li>
                        </ol>
                    </div>
                    <h4 class="page-title">Starter</h4>
                </div>
            </div>
        </div>

        <div class="row justify-content-center">
            <div class="col-lg-4">
                <div class="text-center">

                    <h1 class="text-error mt-4">403</h1>
                    <h4 class="text-uppercase text-danger mt-3">Forbbiden</h4>
                    <p class="text-muted mt-3">Access to this resources on the server is denied</p>
                </div> <!-- end /.text-center-->
            </div> <!-- end col-->
        </div>

    </div> <!-- container -->

</div> <!-- content -->

@endsection
