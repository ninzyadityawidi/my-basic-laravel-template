<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="shortcut icon" href="{{ asset('attex/images/favicon.ico') }}">
    <title>@yield('title', 'Moratel')</title>


    <link href="{{ asset('attex/vendor/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />

    <!-- Theme Config Js -->
    <script src="{{ asset('attex/js/config.js') }}"></script>

    <!-- App css -->
    <link href="{{ asset('attex/css/app.min.css') }}" rel="stylesheet" type="text/css" id="app-style" />

    <!-- Icons css -->
    <link href="{{ asset('attex/css/icons.min.css') }}" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="{{ asset('css/icons.min.css') }}">

    <link rel="stylesheet" href="{{ asset('libs/toastr/toastr.min.css') }}">

    <script>
        var token = "{{ csrf_token() }}";

        function getDepartment(id = null, html = null, setDefault = null) {
            $.ajax({
                url: "{{ url('user/find/department') }}",
                method: "GET",
                data: {
                    id: id
                },
                success: function (data) {
                    $(html).empty();
                    $(html).append('<option value="">Select Department</option>');
                    $.each(data, function (index, value) {
                        let option = `<option value="${value.id}">${value.name}</option>`;
                        $(html).append(option);
                    });

                    if (setDefault == 1) {
                        $(html).val(id);
                    }
                }
            });
        }

        function getTeams(id = null, html = null, setDefault = null) {
            $.ajax({
                url: "{{ url('user/find/team') }}",
                method: "GET",
                data: {
                    id: id
                },
                success: function (data) {
                    $(html).empty();
                    $(html).append('<option value="">Select Team</option>');
                    $.each(data, function (index, value) {
                        let option = `<option value="${value.id}">${value.name}</option>`;
                        $(html).append(option);
                    });

                    if (setDefault == 1) {
                        $(html).val(id);
                    }
                }
            });
        }

    </script>

    @yield('additionalCss')

    @vite([
    'resources/js/global'
    ])
</head>

<body>


    <div class="wrapper">

        @include('layouts.attex.partials.topbar')


        @include('layouts.attex.partials.sidebar')

        <div class="content-page">

            @yield('content')


            @include('layouts.attex.partials.footer')

        </div>


    </div>

    @include('layouts.attex.partials.right_sidebar')


    <script src="{{ asset('attex/js/vendor.min.js') }}"></script>

    <script src="{{ asset('libs/toastr/toastr.min.js') }}"></script>

    <script>
        function actionQuestion(id, uri, textQuestion, textConfirm, type) {
            Swal.fire({
                title: textQuestion,
                showDenyButton: true,
                showCancelButton: true,
                confirmButtonText: textConfirm,
                denyButtonText: `No`,
                icon: "question"
            }).then((result) => {
                /* Read more about isConfirmed, isDenied below */
                if (result.isConfirmed) {
                    $.ajax({
                        url: uri,
                        data: {
                            id: id,
                            "_token": token,
                        },
                        type: type,
                        dataType: 'JSON',
                        beforeSend: function () {
                            $('.btn-form').prop('disabled', true);
                        },
                        success: function (obj) {
                            if (obj.success) {
                                toastr.success(obj.msg);
                                if (obj.callback != undefined) {
                                    window.location.href = obj.callback;
                                }
                            } else {
                                if (obj.validation != undefined) {
                                    $.each(obj.msg, function (key, value) {
                                        toastr.error(value);
                                    });
                                } else {
                                    toastr.error(obj.msg);
                                }

                            }
                        },
                        complete: function () {
                            $('.btn-form').prop('disabled', false);
                        }
                    })
                } else if (result.isDenied) {
                    Swal.fire("Changes are not saved", "", "info");
                }
            });
        }

    </script>


    <script src="{{ asset('attex/vendor/select2/js/select2.min.js') }}"></script>

    @yield('additionalJs')

    <script>
        $('.mclose').on('click', ()=>{
            $('.modal').modal('hide');
        });
    </script>


    <script src="{{ asset('attex/js/app.min.js') }}"></script>

</body>

</html>
