<!-- ========== Left Sidebar Start ========== -->
<div class="leftside-menu">
    <!-- Brand Logo Light -->
    <a href="index.php" class="logo logo-light text-white py-3">
        <h3>Master-App</h3>
    </a>

    <!-- Sidebar Hover Menu Toggle Button -->
    <div class="button-sm-hover" data-bs-toggle="tooltip" data-bs-placement="right" title="Show Full Sidebar">
        <i class="ri-checkbox-blank-circle-line align-middle"></i>
    </div>

    <!-- Full Sidebar Menu Close Button -->
    <div class="button-close-fullsidebar">
        <i class="ri-close-fill align-middle"></i>
    </div>

    <!-- Sidebar -left -->
    <div class="h-100" id="leftside-menu-container" data-simplebar>
        <!-- Leftbar User -->
        <div class="leftbar-user">
            <a href="pages-profile.php">
                <img src="{{ asset('attex/images/users/avatar-1.jpg') }}" alt="user-image" height="42"
                    class="rounded-circle shadow-sm" />
                <span class="leftbar-user-name mt-2">Tosha Minner</span>
            </a>
        </div>

        <!--- Sidemenu -->
        <ul class="side-nav">

            <li class="side-nav-title">Main</li>

            <li class="side-nav-item">
                <a href="{{ route('dashboard') }}" class="side-nav-link">
                    <i class="ri-home-4-line"></i>
                    <span> Dashboard </span>
                </a>
            </li>

            @if (hasPermission('MASTER'))
                <li class="side-nav-title">Master</li>

                @if (hasPermission('MASTER [COMPANY]'))
                    <li class="side-nav-item">
                        <a href="{{ route('company.index') }}" class="side-nav-link">
                            <i class="ri-building-4-line"></i>
                            <span> Company </span>
                        </a>
                    </li>
                @endif

                @if (hasPermission('MASTER [DEPARTMENT]'))
                    <li class="side-nav-item">
                        <a href="{{ route('department.index') }}" class="side-nav-link">
                            <i class="ri-government-line "></i>
                            <span> Department </span>
                        </a>
                    </li>
                @endif

                @if (hasPermission('MASTER [TEAM]'))
                    <li class="side-nav-item">
                        <a href="{{ route('team.index') }}" class="side-nav-link">
                            <i class="ri-account-box-line "></i>
                            <span> Team </span>
                        </a>
                    </li>
                @endif

                @if (hasPermission('MASTER [ROLE]'))
                    <li class="side-nav-item">
                        <a href="{{ route('role.index') }}" class="side-nav-link">
                            <i class="ri-lock-line "></i>
                            <span> Role </span>
                        </a>
                    </li>
                @endif

                @if (hasPermission('MASTER [USER]'))
                    <li class="side-nav-item">
                        <a href="{{ route('user.index') }}" class="side-nav-link">
                            <i class="ri-user-line"></i>
                            <span> User </span>
                        </a>
                    </li>
                @endif
            @endif
            @if (hasPermission('SETTING'))
                <li class="side-nav-title">Setting</li>
            @endif
            
            {{-- example sub menu --}}
            {{-- <li class="side-nav-item">
                <a data-bs-toggle="collapse" href="#sidebarTasks" aria-expanded="false" aria-controls="sidebarTasks" class="side-nav-link">
                    <i class="ri-task-line"></i>
                    <span> Tasks </span>
                    <span class="menu-arrow"></span>
                </a>
                <div class="collapse" id="sidebarTasks">
                    <ul class="side-nav-second-level">
                        <li>
                            <a href="https://attex-l.getappui.com/task/list">List</a>
                        </li>
                        <li>
                            <a href="https://attex-l.getappui.com/task/details">Details</a>
                        </li>
                    </ul>
                </div>
            </li> --}}
            

            @if (hasPermission('SETTING [MODULE]'))
                <li class="side-nav-item">
                    <a href="{{ route('module.index') }}" class="side-nav-link">
                        <i class="ri-file-line"></i>
                        <span> Module </span>
                    </a>
                </li>
            @endif

            @if (hasPermission('SETTING [PERMISSION]'))
                <li class="side-nav-item">
                    <a href="{{ route('permission.index') }}" class="side-nav-link">
                        <i class="ri-tools-line"></i>
                        <span> Permission </span>
                    </a>
                </li>
            @endif
        </ul>
        <!--- End Sidemenu -->

        <div class="clearfix"></div>
    </div>
</div>
<!-- ========== Left Sidebar End ========== -->
